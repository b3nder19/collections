package ru.b3nder.HashMap;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyHashMapTest {
    @Test
    public void testMyHashMap() {
        MyHashMap<Integer, String> map = new MyHashMap<>();

        map.put(1, "A");
        map.put(2, "B");
        map.put(3, "C");
        map.put(4, "D");

        String actual = "C";

        assertEquals(actual, map.get(3));
    }

    @Test
    public void testSizeMap() {
        MyHashMap<Integer, String> map = new MyHashMap<>();

        map.put(1, "A");
        map.put(2, "B");
        map.put(3, "C");
        map.put(4, "D");

        map.remove(2);

        int actual = 3;

        assertEquals(actual, map.size());
    }
}