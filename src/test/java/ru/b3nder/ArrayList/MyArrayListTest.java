package ru.b3nder.ArrayList;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyArrayListTest {
    @Test
    public void testSizeArrayList() {
        MyArrayList<String> listOfStrings = new MyArrayList<>();
        listOfStrings.add("Hello");
        listOfStrings.add("World!");
        listOfStrings.add("Test");
        listOfStrings.add("teSt");

        listOfStrings.remove(2);

        int actual = 3;

        assertEquals(actual, listOfStrings.size());
    }

}