package ru.b3nder.ArrayList;

public interface List<T> {
    void add(T object);

    T set(int index, T object);

    T get(int index);

    void remove(int index);

    int size();
}
