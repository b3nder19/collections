package ru.b3nder.ArrayList;

public class MyArrayList<T> implements List<T>{
    private static final int INIT_CAPACITY = 10;
    private Object[] elementData = new Object[INIT_CAPACITY];
    private int currentSize = 0;

    @Override
    public void add(T object) {
        if (elementData.length - 1 == currentSize) {
            increaseListSize(elementData.length * 2);
        }
        elementData[currentSize] = object;
        currentSize++;
    }

    @Override
    public T set(int index, T object) {
        T oldValue = (T) elementData[index];
        elementData[index] = object;
        return oldValue;
    }

    @Override
    public T get(int index) {
        return (T) elementData[index];
    }

    @Override
    public void remove(int index) {
        for (int i = index; i < currentSize; i++) {
            elementData[i] = elementData[i + 1];
        }
        elementData[currentSize] = null;
        currentSize--;
    }

    @Override
    public int size() {
        return currentSize;
    }

    public void increaseListSize(int newLength) {
        Object[] newArray = new Object[newLength];
        System.arraycopy(elementData, 0, newArray, 0, currentSize);
        elementData = newArray;
    }
}
