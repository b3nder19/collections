package ru.b3nder.HashSet;

public interface Set<T> {
    void add(T object);

    T remove(T object);

    int size();
}
