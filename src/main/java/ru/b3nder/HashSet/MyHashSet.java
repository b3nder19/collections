package ru.b3nder.HashSet;

import java.util.NoSuchElementException;

public class MyHashSet<T> implements Set<T>{

    private static final Integer INITIAL_CAPACITY = 1 << 4; // 16

    private final Node<T>[] buckets;

    static class Node<T> {
        T data;
        Node<T> next;

        Node(T data) {
            this.data = data;
            this.next = null;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Node<T> getNext() {
            return next;
        }

        public void setNext(Node<T> next) {
            this.next = next;
        }
    }

    private int size;

    public MyHashSet(final int capacity) {
        this.buckets = new Node[capacity];
        this.size = 0;
    }

    public MyHashSet() {
        this(INITIAL_CAPACITY);
    }

    @Override
    public void add(T t) {
        int index = hashCode() % buckets.length;

        Node<T> bucket = buckets[index];

        Node<T> newNode = new Node<>(t);

        if (bucket == null) {
            buckets[index] = newNode;
            size++;
            return;
        }

        while (bucket.next != null) {
            if (bucket.data.equals(t)) {
                return;
            }
            bucket = bucket.next;
        }


        if (!bucket.data.equals(t)) {
            bucket.next = newNode;
            size++;
        }
    }

    @Override
    public T remove(T t) {
        int index = hashCode() % buckets.length;

        Node<T> bucket = buckets[index];

        if (bucket == null) {
            throw new NoSuchElementException("No Element Found");
        }

        if (bucket.data.equals(t)) {
            buckets[index] = bucket.next;
            size--;
            return t;
        }

        Node<T> prev = bucket;

        while (bucket != null) {
            if (bucket.data.equals(t)) {
                prev.next = bucket.next;
                size--;
                return t;
            }
            prev = bucket;
            bucket = bucket.next;
        }
        return null;
    }

    @Override
    public int size() {
        return size;
    }
}


