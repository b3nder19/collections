package ru.b3nder.HashMap;

public interface Map<K, V> {
    void put(K key, V value);

    V get(Object key);

    void remove(Object key);

    int size();
}
