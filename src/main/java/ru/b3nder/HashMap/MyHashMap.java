package ru.b3nder.HashMap;

public class MyHashMap<K, V> implements Map<K, V> {
    private static final int INITIAL_CAPACITY = 16;
    private final Node<K, V>[] table = new Node[INITIAL_CAPACITY];
    private int size = 0;

    static class Node<K, V> {
        final K key;
        V value;
        Node<K, V> next;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }

        public Node<K, V> getNext() {
            return next;
        }

        public void setNext(Node<K, V> next) {
            this.next = next;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void put(K key, V value) {
        int keyHash = key.hashCode();
        int hash = getSupplementalHash(keyHash);

        int bucket = getBucketNumber(hash);

        Node<K, V> entryElement = table[bucket];

        if (entryElement != null) {
            if (entryElement.key.equals(key)) {
                entryElement.value = value;
            } else {
                while (entryElement.next != null) {
                    entryElement = entryElement.next;
                }
                entryElement.next = new Node<>(key, value);
            }
        } else {
            Node<K, V> entryInNewBucket = new Node<>(key, value);
            table[bucket] = entryInNewBucket;
            size++;
        }
    }

    @Override
    public V get(Object key) {
        int hash = getSupplementalHash(key.hashCode());
        int bucket = getBucketNumber(hash);

        Node<K, V>  entryElement = table[bucket];

        while (entryElement != null) {
            if (entryElement.key.equals(key)) {
                return entryElement.getValue();
            }
            entryElement = entryElement.next;
        }
        return null;
    }

    @Override
    public void remove(Object key) {
        int hash = getSupplementalHash(key.hashCode());
        int bucket = getBucketNumber(hash);

        Node<K, V>  previous = null;
        Node<K, V>  entryElement = table[bucket];

        while (entryElement != null) {
            if (entryElement.key.equals(key)) {
                entryElement = entryElement.next;
                table[bucket] = entryElement;
                size--;
                return;
            }
        }
        entryElement = entryElement.next;
    }

    private int getSupplementalHash(int h) {
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }

    private int getBucketNumber(int hash) {
        return hash & (INITIAL_CAPACITY - 1);
    }
}